import cv2
import threading
from http.server import BaseHTTPRequestHandler, HTTPServer
from PIL import Image
from socketserver import ThreadingMixIn 
from io import BytesIO
import time

# global variables
cap = None
class VideoRequestHandler(BaseHTTPRequestHandler):
    global cap
    def do_GET(self):
        self.send_response(200)
        self.send_header('Content-type', 'multipart/x-mixed-replace; boundary=--jpgboundary')
        self.end_headers()
        while True:
            ret, frame = cap.read()
            if ret:
                ok, encoded = cv2.imencode('.jpg', frame)
                self.wfile.write("--jpgboundary".encode())
                self.send_header('Content-type', 'image/jpeg')
                self.send_header('Content-length', str(len(encoded)))
                self.end_headers()
                self.wfile.write(encoded)
                self.end_headers()

class ThreadedHTTPServer(ThreadingMixIn, HTTPServer):
    pass

def main():
    global cap
    cap = cv2.VideoCapture(0)

    IP = 'localhost'
    PORT = 8090

    try:
        http_server = ThreadedHTTPServer((IP, PORT), VideoRequestHandler)
        th1 = threading.Thread(target=http_server.serve_forever)
        th1.start()
        print("Started stream on: localhost:8090")

    except Exception as e:
        print(e)

    except KeyboardInterrupt:
        cap.release()
        http_server.socket.close()

if __name__ == "__main__":
    main()